<?php

class WP_LDAP_AUTH
{
	function disable_password_reset() 
	{ 
		return false; 
	}
	function remove_password_reset() 
	{ 
		add_filter( 'gettext', array( 'WP_LDAP_AUTH', 'remove_password_reset_text' ) ); 
	}
	function remove_password_reset_text( $text ) 
	{ 
		if ( $text == 'Lost your password?' ) { $text = ''; } return $text;
	}
	
	# Initialise admin page for plugin options
	#-------------------------------------------------------------------------
	function wp_ldap_manage_init()
	{
		add_options_page( 'WP LDAP Authentication', 'WP LDAP Auth', 5, basename(__FILE__), 'wp_ldap_options');
	}
	# Save default plugin options in WordPress Options table
	#-------------------------------------------------------------------------
	function wp_ldap_init()
	{
		$options = array();
		$options['DN'] = '';
		$options['restrict_dn'] = 0;
		$options['ldap_server'] = '[insert default]';
				
		if( !get_option('wp_ldap_auth') ){
			add_option( 'wp_ldap_auth', $options );
		}
	}
	# Update/create administration page for managing plugin options
	#-------------------------------------------------------------------------
	function wp_ldap_options()
	{
		if($_POST['action'] == 'update')
		{	
			$options = array();
			$options['restrict_dn'] = $_POST['restrict_dn'];
			$options['DN'] = $_POST['DN'];
			$options['ldap_server'] = $_POST['ldap_server'];
			
	        update_option( 'wp_ldap_auth', $options );
	?>
	        <div class="updated"><p><strong><?php _e('Options saved.', 'eg_trans_domain' ); ?></strong></p></div>
	<?php
		}
		$options = array(); 
		$options = get_option('wp_ldap_auth');
	?>
		<div class="wrap">
			<h2>WP LDAP Auth Options</h2>
			<div class="metabox-holder">
				<div class="postbox">
			      	<h3>Settings</h3>
					<div class="inside">
						<form method="post">
						<?php wp_nonce_field('update-options'); ?>
						<table class="form-table">           
							<tr valign="top">
								<th scope="row">Restrict DN?</th>
								<td>
								 Yes <input type="radio" name="restrict_dn" value="1"<?php if($options['restrict_dn']==1) echo ' checked="checked"';?> />
								 No <input type="radio" name="restrict_dn" value="0"<?php if($options['restrict_dn']!=1) echo ' checked="checked"';?> />
								</td>
							</tr>
							<tr valign="top">
								<th scope="row">Which DN do you not want to allow? (eg, ou=STU)</th>
								<td>
								 <input type="text" name="DN" value="<?php echo $options['DN']; ?>">
								</td>
							</tr>
							<tr valign="top">
								<th scope="row">LDAP Server:</th>
								<td>
									<input type="text" name="ldap_server" value="<?php echo $options['ldap_server']; ?>">
								</td>
							</tr>
						</table>
						<input type="hidden" name="action" value="update" />
						<input type="hidden" name="page_options" value="restrict_dn,DN" />
						<p class="submit" style="margin-left:10px;">
							<input type="submit" name="Submit" value="<?php _e('Save Changes') ?>" />
						</p>
						</form>
					</div>
				      
				 </div>
			 </div>
		</div>
	<?php
	}
	# Replace WordPress loging banner with a custom banner
	#-------------------------------------------------------------------------
	function custom_login_banner() 
	{ // Adds custom banner to the login page - modify 'loginBanner.jpg' in the plugin directory. PSD is also available.
	?>	
		<style type="text/css">
	        h1 a { background-image:url(<?php echo WP_PLUGIN_URL; ?>/wp_ldap_auth/loginBanner.jpg) !important; }
	    </style>
	<?php
	}
	# Appropriate errors upon LDAP response
	#-------------------------------------------------------------------------
	function ldap_errors($username, $id)
	{	
		switch($id)
		{
			case 1:
				$user = get_userdatabylogin($username);
				return new WP_User($user->ID);
			break;
			case 2:
				return new WP_Error('incorrent_password', __('Incorrect password. Please ensure you are using your MMU network ID/password.'));	
			break;
			case 3:
				return new WP_Error('incorrent_password', __('Incorrect username. Please ensure you are using your MMU network ID/password.'));	
			break;
			case 4:
				return new WP_Error('incorrent_password', __('Authentication error. If the error persists, please notify your web development officer.'));	
			break;
			case 5:
				return new WP_Error('unauthorised', __('Authentication error. You do not have sufficient permissions to login to this website.'));
			break;
		}
	}
	# Upon LDAP authentication, create a WP user for allocating permissions
	#-------------------------------------------------------------------------
	function create_wp_user($username, $info)
	{	
		$userData = array(
					'user_pass'     => microtime(),
					'user_login'    => $username,
					'user_nicename' => $info[0]['givenname'][0].' '.$info[0]['sn'][0],
					'user_email'    => $info[0]['mail'][0],
					'display_name'  => $info[0]['givenname'][0].' '.$info[0]['sn'][0],
					'first_name'    => $info[0]['givenname'][0],
					'last_name'     => $info[0]['sn'][0],
					'role'          => 'subscriber'
					);
		//Get ID of new user
		if ( wp_insert_user($userData) )
		{
			return( 1 );
		}
	}
	# Search the LDAP tree for the user
	#-------------------------------------------------------------------------
	function search_tree($username, $password, $newuser)
	{ #  search the LDAP tree for user and return some attributes
		$options = array();
		$options = get_option( 'wp_ldap_auth' );
		if ( $connection = ldap_connect( LDAP_CONN ) ) 
		{
			$ldapbind = ldap_bind( $connection, "cn=ldaproxy,o=mmu", "");
			$filter = "cn=$username";
			$objects = array( "ou", "sn", "givenname", "mail" );
	        if ( $search_result = @ldap_search( $connection ,"o=mmu", "$filter", $objects ) )
	        { 
	            $info = @ldap_get_entries( $connection , $search_result );
	            if ( $info['count'] == 0 ) 
	            { //no users found
	            	return( 3 );
	        	}
	        	else
	        	{ // single user found
	            	$dn = $info[0]['dn']; # single user on system, no context need be supplied
	            	$dn_option = $options['DN'];
	            	if ( $options['restrict_dn'] == 1 && preg_match( "/$dn_option/", $dn ) )
	            	{
	            		return( 5 );
	            	}
	            	else
	            	{      	                
		            	# authenticated rebind to LDAP server using users details instead of proxy.
			            if ( !@ldap_bind( $connection , "$dn", $password ) ) 
			            {
			            	# user ok but password incorrect (more correctly - unable to perform an authenticed bind).
				            return( 2 );
		    	        }
		    	        else
		    	        {
		    	        	if ( $newuser == 1 )
		    	        	{
		    	        		#create wp user
		    	        		$user_created = create_wp_user($username, $info);
								if ( $user_created == 1 )
								{
									session_start();
									$_SESSION['dn'] = $info[0]['dn'];
									return( 1 );
								}
		    	        	}
		    	        	else
		    	        	{    	        	
								# user found and password ok
								session_start();
								$_SESSION['dn'] = $info[0]['dn'];
								return( 1 ); 
							}  
		            	}
		            }
	        	}
	        }
	        else
	        {
	            return( 4 ); # there was an error searching the tree.
	        }
		}
	}
}