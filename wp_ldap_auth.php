<?php
/*
Plugin Name: WP LDAP Auth
Plugin URI: http://www.hlss.mmu.ac.uk
Description:  Authenticates Wordpress usernames against LDAP.
Version: 1.6
Author: Phil Kershaw; Neil Ashdown; Steve Cooke
Author URI: http://blog.philkershaw.me
*/
#--------------------------------------------------------------------------
#
#   Wordpress LDAP Authentication Plugin
#
#   Phil Kershaw, P.Kershaw@mmu.ac.uk
#   Faculty of Humanities, Law and Social Science - Manchester Metropolitan University
#
#	TODO: 
#	-	Create admin panel for easy upating of LDAP settings - done
#	-	Refactor code - done
#	-	Add functionality for Active Directory
#	-	Add functionality for restricting access to specific pages to stated OUs
#	-	Add more options...
#
#	CHANGE LOG:
#	-	Fixed issue when a user is created the role wasn't set. (22/06/2010)
#
#--------------------------------------------------------------------------
require('wp_ldap_auth.class.php');
global $LdapAuth;
$LdapAuth = new WP_LDAP_AUTH;
# Define canstant(s) and WordPress hooks
#--------------------------------------------------------------------------
$options = array();
$options = get_option( 'wp_ldap_auth' );
define(LDAP_CONN, $options['ldap_server']);
add_action( 'login_head', array( $LdapAuth, 'remove_password_reset' ) );
add_action( 'admin_menu', array( $LdapAuth, 'wp_ldap_manage_init' ) );
add_action( 'login_head', array( $LdapAuth, 'custom_login_banner' ) );
add_filter( 'allow_password_reset', array( $LdapAuth, 'disable_password_reset' ) );
register_activation_hook( __FILE__, array( $LdapAuth, 'wp_ldap_init' ) );
# Re-define wp_authenticate()
#-------------------------------------------------------------------------- 
if ( !function_exists('wp_authenticate') ) :
/**
 * Checks a user's login information and logs them in if it checks out.
 *
 * @since 2.5.0
 *
 * @param string $username User's username
 * @param string $password User's password
 * @return WP_Error|WP_User WP_User object if login successful, otherwise WP_Error object.
 */
function wp_authenticate($username, $password) 
{
	#$new_redirect_to = $_SERVER['HTTP_REFERER'];
	#add_filter('login_redirect',$new_redirect_to, 10, 3);
	global $LdapAuth;
	$username = sanitize_user($username);
	$password = trim($password);	
	$user = apply_filters('authenticate', null, $username, $password);
	if ( is_wp_error($user) ):
		if ( $user->get_error_code() == 'invalid_username' ){ # user not found in Wordpress DB
			#$LDAP_Search = search_tree( $username, $password );
			// Search LDAP Directory for user and authenticate
			$ldap = $LdapAuth->search_tree($username, $password, 1);
			$user = $LdapAuth->ldap_errors($username, $ldap);
		}
		elseif ( $user->get_error_code() == 'incorrect_password' )
		{
			$ldap = $LdapAuth->search_tree($username, $password, 0);
			$user = $LdapAuth->ldap_errors($username, $ldap);
		}
	endif;
	return $user;
}
endif;
?>
