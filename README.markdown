# WordPress LDAP Authentication Plugin

This is a WordPress plugin developed to Authenticate users via [LDAP](http://en.wikipedia.org/wiki/LDAP).

This is a very simple implementation of LDAP authentication for WordPress but feel free to fork and extend to your requirements.